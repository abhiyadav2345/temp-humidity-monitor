/*
 * File Name: app_json_utils.c
 * Description:
 *
 *  Created on: 25-May-2019
 *      Author: Noyel Seth
 */

#include "app_json_utils.h"

#include "parson.h"
#include "core_utils.h"

#if CONFIG_USE_LOGGING
#include "core_logger.h"
#include "logger_constants.h"
#endif

#include <string.h>
#include <stdlib.h>
#include <math.h>

#define TAG "app_json_utils"

/****************************************************************************************************************************/

/*
 * Creating Application POST Json String:
 *	{
 *		"temperature_in_celsius": 50,
 *		"temperature_in_fahrenheit": 122
 *	}
 */
core_err make_temp_publish_json(TEMP_HUMIDITY_PUBLISH_DATA *publish_data, bool temp_publish_in_celsius,
		bool temp_publish_in_fahrenheit, char **ppbuf, size_t *plen) {
	core_err ret = CORE_FAIL;

	char *serialized_string = NULL;

	if (NULL != publish_data) {
		// root obj
		JSON_Value *root_value = json_value_init_object();
		JSON_Object *root_object = json_value_get_object(root_value);

		if (temp_publish_in_celsius == true) {
#if 0
			if (json_object_set_number(root_object,
					GET_VAR_NAME(publish_data->temperature_data.temperature_in_celsius, "."),
					publish_data->temperature_data.temperature_in_celsius) == JSONSuccess) {

			}
#else
			char buf[10] = { 0x00 };
			sprintf(buf, "%0.2f", publish_data->temperature_data.temperature_in_celsius);
			if (json_object_set_string(root_object,
					GET_VAR_NAME(publish_data->temperature_data.temperature_in_celsius, "."), buf) == JSONSuccess) {

			}
#endif
		}
		if (temp_publish_in_fahrenheit == true) {
#if 0
			if (json_object_set_number(root_object,
					GET_VAR_NAME(publish_data->temperature_data.temperature_in_fahrenheit, "."),
					publish_data->temperature_data.temperature_in_fahrenheit) == JSONSuccess) {

			}
#else
			char buf[10] = { 0x00 };
			sprintf(buf, "%0.2f", publish_data->temperature_data.temperature_in_fahrenheit);
			if (json_object_set_string(root_object,
					GET_VAR_NAME(publish_data->temperature_data.temperature_in_fahrenheit, "."), buf) == JSONSuccess) {

			}
#endif
		}

		serialized_string = json_serialize_to_string(root_value);
		size_t len = json_serialization_size(root_value);

//		serialized_string = json_serialize_to_string_pretty(root_value);
//		size_t len = json_serialization_size_pretty(root_value);

		len = len + 1;  // since json_serialization_size returns size + 1

		char *ptemp = (char*) calloc(len, sizeof(char));
		if (ptemp == NULL) {
			CORE_LOGE(TAG, "Failed to create application's Post Json for Low Memory");
			*ppbuf = NULL;
			*plen = 0;

			json_value_free(root_value);
			if (serialized_string != NULL) {
				json_free_serialized_string(serialized_string);
			}

			return CORE_ERR_NO_MEM;
		}

		memset(ptemp, 0x00, len);
		memcpy(ptemp, serialized_string, len);
		*plen = len;
		*ppbuf = ptemp;

		json_value_free(root_value);
		if (serialized_string != NULL) {
			json_free_serialized_string(serialized_string);
		}
		ret = CORE_OK;
	} else {
		ret = CORE_ERR_INVALID_ARG;
	}
	return ret;

}

/*
 * Creating Application POST Json String:
 *
 *	{
 *		"humidity": 50
 *	}
 *
 *
 */
core_err make_humidity_publish_json(TEMP_HUMIDITY_PUBLISH_DATA *publish_data, char **ppbuf, size_t *plen) {
	core_err ret = CORE_FAIL;

	char *serialized_string = NULL;

	if (NULL != publish_data) {
		// root obj
		JSON_Value *root_value = json_value_init_object();
		JSON_Object *root_object = json_value_get_object(root_value);
#if 0
		if (json_object_set_number(root_object, GET_VAR_NAME(publish_data->humidity_data.humidity, "."),
				publish_data->humidity_data.humidity) == JSONSuccess) {

		}
#else
		char buf[10] = { 0x00 };
		sprintf(buf, "%0.2f", publish_data->humidity_data.humidity);
		if (json_object_set_string(root_object, GET_VAR_NAME(publish_data->humidity_data.humidity, "."), buf)
				== JSONSuccess) {

		}
#endif
		serialized_string = json_serialize_to_string(root_value);
		size_t len = json_serialization_size(root_value);

//		serialized_string = json_serialize_to_string_pretty(root_value);
//		size_t len = json_serialization_size_pretty(root_value);

		len = len + 1;  // since json_serialization_size returns size + 1

		char *ptemp = (char*) calloc(len, sizeof(char));
		if (ptemp == NULL) {
			CORE_LOGE(TAG, "Failed to create application's Post Json for Low Memory");
			*ppbuf = NULL;
			*plen = 0;

			json_value_free(root_value);
			if (serialized_string != NULL) {
				json_free_serialized_string(serialized_string);
			}
			return CORE_ERR_NO_MEM;
		}

		memset(ptemp, 0x00, len);
		memcpy(ptemp, serialized_string, len);
		*plen = len;
		*ppbuf = ptemp;

		json_value_free(root_value);
		if (serialized_string != NULL) {
			json_free_serialized_string(serialized_string);
		}
		ret = CORE_OK;
	} else {
		ret = CORE_ERR_INVALID_ARG;
	}
	return ret;

}

/*
 * Parse Application Config's response Json String:
 * {
 * 	"publish_temp" : true,
 * 	"publish_humidity" : true,
 * 	"read_dht22_interval" : 500,
 * 	"temp_publish_interval" : 1000,
 * 	"humidity_publish_interval" : 2000,
 * 	"temp_publish_unit" : 2
 * }
 *
 */

core_err parse_config_json(char *string, TEMP_HUMIDITY_APP_CONFIGURATION_DATA *resp) {
	core_err ret = CORE_FAIL;

	if (NULL != string) {
		JSON_Value *root_value;
		JSON_Object *root_object;
		root_value = json_parse_string(string);

		if (json_value_get_type(root_value) != JSONObject) {
			CORE_LOGE(TAG, "Config Respons JSON Value type not matched\r\n");
			json_value_free(root_value);
			return ret;
		}

		root_object = json_value_get_object(root_value);
		JSON_Value *keys;

		keys = json_object_get_value(root_object, GET_VAR_NAME(resp->publish_temp, "->"));
		if (json_value_get_type(keys) == JSONNumber) {

			resp->publish_temp = json_object_get_number(root_object, GET_VAR_NAME(resp->publish_temp, "->"));
		}

		keys = json_object_get_value(root_object, GET_VAR_NAME(resp->publish_humidity, "->"));
		if (json_value_get_type(keys) == JSONNumber) {

			resp->publish_humidity = json_object_get_number(root_object, GET_VAR_NAME(resp->publish_humidity, "->"));
		}

		keys = json_object_get_value(root_object, GET_VAR_NAME(resp->read_dht22_interval, "->"));
		if (json_value_get_type(keys) == JSONNumber) {
			resp->read_dht22_interval = json_object_get_number(root_object,
					GET_VAR_NAME(resp->read_dht22_interval, "->"));
		}

		keys = json_object_get_value(root_object, GET_VAR_NAME(resp->temp_publish_interval, "->"));
		if (json_value_get_type(keys) == JSONNumber) {
			resp->temp_publish_interval = json_object_get_number(root_object,
					GET_VAR_NAME(resp->temp_publish_interval, "->"));
		}

		keys = json_object_get_value(root_object, GET_VAR_NAME(resp->humidity_publish_interval, "->"));
		if (json_value_get_type(keys) == JSONNumber) {
			resp->humidity_publish_interval = json_object_get_number(root_object,
					GET_VAR_NAME(resp->humidity_publish_interval, "->"));
		}

		keys = json_object_get_value(root_object, GET_VAR_NAME(resp->temp_publish_unit, "->"));
		if (json_value_get_type(keys) == JSONNumber) {

			resp->temp_publish_unit = json_object_get_number(root_object, GET_VAR_NAME(resp->temp_publish_unit, "->"));

			switch (resp->temp_publish_unit) {
			case TEMPERATURE_PUBLISH_IN_CELSIUS: {
				resp->temp_publish_in_celsius = true;
				resp->temp_publish_in_fahrenheit = false;
				break;
			}
			case TEMPERATURE_PUBLISH_IN_FAHRENHEIT: {
				resp->temp_publish_in_celsius = false;
				resp->temp_publish_in_fahrenheit = true;
				break;
			}
			case TEMPERATURE_PUBLISH_IN_CELSIUS_AND_FAHRENHEIT: {
				resp->temp_publish_in_celsius = true;
				resp->temp_publish_in_fahrenheit = true;
				break;

			}
			default: {
				resp->temp_publish_in_celsius = false;
				resp->temp_publish_in_fahrenheit = false;
				break;
			}
			}
		}

		ret = CORE_OK;
		json_value_free(root_value);

	} else {
		ret = CORE_ERR_INVALID_RESPONSE;
	}
	return ret;

}

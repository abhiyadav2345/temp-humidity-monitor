
#include "temp-humidity-app/temp_humidity_app.h"

#define TAG	"core_app_main"

void core_app_main(void * param)
{
	CORE_LOGI(TAG, "==================================================================");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "Starting application built on the Iquester Core Embedded Framework");
	CORE_LOGI(TAG,"");
	CORE_LOGI(TAG, "==================================================================");

	CORE_LOGI(TAG, "Calling your_module_init() in your_module.c in your-module directory ...");
	temp_humidity_init();
	CORE_LOGI(TAG, "Returned from your_module_init()");

	while(1){
		temp_humidity_loop();
		TaskDelay(DELAY_500_MSEC / TICK_RATE_TO_MS);
	}
}
